package ir.snapppay.moneyTracker.launcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


/**
 * This class is responsible for application startup.
 * @author Behrouz Takhti
 * @version 0.0.1-SNAPSHOT
 * @since 2024-01-14
 */
@SpringBootApplication
@EntityScan(basePackages = {"ir.snapppay.moneyTracker.persistence.domain"})
@EnableJpaRepositories(basePackages = {"ir.snapppay.moneyTracker.persistence.repository"})
@ComponentScan(value = "ir.snapppay.moneyTracker")
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
public class LaunchApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(LaunchApplication.class);
    public static void main(String[] args){
        SpringApplication.run(LaunchApplication.class);
        LOGGER.info("moneyTracker application started successfully !");
    }
}
