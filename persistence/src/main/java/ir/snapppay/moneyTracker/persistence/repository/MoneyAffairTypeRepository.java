package ir.snapppay.moneyTracker.persistence.repository;

import ir.snapppay.moneyTracker.persistence.domain.MoneyAffairType;
import ir.snapppay.moneyTracker.persistence.types.MoneyTypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * This Repository is responsible for Money affair type processing.
 * @author Behrouz Takhti
 * @version 0.0.2-SNAPSHOT
 * @since 2024-01-15
 */
@Repository
public interface MoneyAffairTypeRepository extends JpaRepository<MoneyAffairType, Long> {
    Optional<MoneyAffairType> findByTypeAndNameAndUserId(MoneyTypeEnum type, String name, Long userId);

}
