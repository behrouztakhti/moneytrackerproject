package ir.snapppay.moneyTracker.persistence.domain;

import ir.snapppay.moneyTracker.persistence.types.MoneyTypeEnum;
import jakarta.persistence.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.List;


/**
 * This is MONEY_AFFAIR_TYPE entity. it has some columns in terms of expense type, and it also has ManyToOne relation with USER table, ond mayToOne relation with MoneyAffair.
 * Every user can create multiple expense type.
 * We also set Jpa auditing listener on this table.
 * @author Behrouz Takhti
 * @version 0.0.2-SNAPSHOT
 * @since 2024-01-15
 */
@Entity
@Table(name = "MONEY_AFFAIR_TYPE")
@EntityListeners(AuditingEntityListener.class)
@SequenceGenerator(name = "moneyAffairTypeSequence", sequenceName = "MONEY_AFFAIR_TYPE_SEQ", initialValue = 1, allocationSize = 1)
public class MoneyAffairType extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "moneyAffairTypeSequence")
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "PERSIAN_NAME")
    private String persianName;
    @Column(name = "NAME")
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "TYPE")
    private MoneyTypeEnum type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MONEY_AFFAIR_TYPE_USR_ID", nullable = false)
    private Users user;

    @OneToMany(mappedBy = "moneyAffairType", fetch = FetchType.LAZY)
    private List<MoneyAffairs> MoneyAffairs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MoneyTypeEnum getType() {
        return type;
    }

    public void setType(MoneyTypeEnum type) {
        this.type = type;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public String getPersianName() {
        return persianName;
    }

    public void setPersianName(String persianName) {
        this.persianName = persianName;
    }

    public List<ir.snapppay.moneyTracker.persistence.domain.MoneyAffairs> getMoneyAffairs() {
        return MoneyAffairs;
    }

    public void setMoneyAffairs(List<ir.snapppay.moneyTracker.persistence.domain.MoneyAffairs> moneyAffairs) {
        MoneyAffairs = moneyAffairs;
    }
}
