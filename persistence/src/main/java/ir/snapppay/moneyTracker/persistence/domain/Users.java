package ir.snapppay.moneyTracker.persistence.domain;

import ir.snapppay.moneyTracker.persistence.types.RoleType;
import jakarta.persistence.*;
import java.util.List;


/**
 * This is USERS entity. it has some columns in terms of user, and it also has OneToMany relation with TOKEN table.
 * @author Behrouz Takhti
 * @version 0.0.1-SNAPSHOT
 * @since 2024-01-14
 */
@Entity
@Table(name = "USERS")
@SequenceGenerator(name = "usersSequence", sequenceName = "USERS_SEQ", initialValue = 1, allocationSize = 1)
public class Users {

    public Users() {
    }
    public Users(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usersSequence")
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "NAME", length = 100, nullable = false)
    private String name;

    @Column(name = "FAMILY", length = 100, nullable = false)
    private String family;

    @Column(name = "USERNAME", unique = true, length = 40, nullable = false)
    private String username;

    @Column(name = "PASSWORD", length = 100, nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    private RoleType role;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Tokens> tokens;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<MoneyAffairType> moneyAffairTypes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RoleType getRole() {
        return role;
    }

    public void setRole(RoleType role) {
        this.role = role;
    }

    public List<Tokens> getTokens() {
        return tokens;
    }

    public void setTokens(List<Tokens> tokens) {
        this.tokens = tokens;
    }

    public List<MoneyAffairType> getMoneyAffairTypes() {
        return moneyAffairTypes;
    }

    public void setMoneyAffairTypes(List<MoneyAffairType> moneyAffairTypes) {
        this.moneyAffairTypes = moneyAffairTypes;
    }
}
