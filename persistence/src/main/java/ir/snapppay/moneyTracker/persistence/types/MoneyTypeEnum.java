package ir.snapppay.moneyTracker.persistence.types;

/**
 * This enum is used for determine the type of the token.
 * @author Behrouz Takhti
 * @version 0.0.2-SNAPSHOT
 * @since 2024-01-15
 * @see ir.snapppay.moneyTracker.persistence.domain.Tokens
 */
public enum MoneyTypeEnum {
    EXPENSE, INCOME;
}
