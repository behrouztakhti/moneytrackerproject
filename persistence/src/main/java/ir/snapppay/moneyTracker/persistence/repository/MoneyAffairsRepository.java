package ir.snapppay.moneyTracker.persistence.repository;

import ir.snapppay.moneyTracker.persistence.domain.MoneyAffairs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;


/**
 * This Repository is responsible for Money affair type processing.
 * @author Behrouz Takhti
 * @version 0.0.2-SNAPSHOT
 * @since 2024-01-15
 */
@Repository
public interface MoneyAffairsRepository extends JpaRepository<MoneyAffairs, Long> {
    List<MoneyAffairs> findByCreatedDateAndMoneyAffairTypeId(LocalDate createdDate, Long moneyAffairTypeId);



    List<MoneyAffairs> findByCreatedDateAndMoneyAffairType_UserId(LocalDate createdDate, Long moneyAffairTypeId);


}
