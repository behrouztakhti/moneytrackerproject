package ir.snapppay.moneyTracker.persistence.repository;

import ir.snapppay.moneyTracker.persistence.domain.Tokens;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import java.util.Optional;


/**
 * This Repository is responsible for token processing such as findByToken and ...
 * @author Behrouz Takhti
 * @version 0.0.1-SNAPSHOT
 * @since 2024-01-14
 */
@Repository
public interface TokenRepository extends JpaRepository<Tokens, Long> {

    Optional<Tokens> findByToken(String token);
    @Modifying
    @Transactional
    void deleteAllByUserId(Long userId);
}
