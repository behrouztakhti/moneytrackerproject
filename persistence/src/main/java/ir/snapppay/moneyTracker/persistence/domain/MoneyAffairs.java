package ir.snapppay.moneyTracker.persistence.domain;

import ir.snapppay.moneyTracker.persistence.types.MoneyTypeEnum;
import jakarta.persistence.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.math.BigDecimal;


/**
 * This is MONEY_AFFAIRS entity. it has some columns in terms of expense or income, and it also has ManyToOne relation with MONEY_AFFAIR_TYPE table.
 * Every user can create multiple money affair.
 * We also set Jpa auditing listener on this table.
 * @author Behrouz Takhti
 * @version 0.0.3-SNAPSHOT
 * @since 2024-01-15
 */
@Entity
@Table(name = "MONEY_AFFAIRS")
@EntityListeners(AuditingEntityListener.class)
@SequenceGenerator(name = "moneyAffairsSequence", sequenceName = "MONEY_AFFAIRS_SEQ", initialValue = 1, allocationSize = 1)
public class MoneyAffairs extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "moneyAffairsSequence")
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "AMOUNT", nullable = false)
    private BigDecimal amount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MONEY_AFFAIR_TYPE_ID", nullable = false)
    private MoneyAffairType moneyAffairType;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public MoneyAffairType getMoneyAffairType() {
        return moneyAffairType;
    }

    public void setMoneyAffairType(MoneyAffairType moneyAffairType) {
        this.moneyAffairType = moneyAffairType;
    }
}
