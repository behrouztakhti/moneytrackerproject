package ir.snapppay.moneyTracker.persistence.domain;

import ir.snapppay.moneyTracker.persistence.types.TokenType;
import jakarta.persistence.*;


/**
 * This is TOKENS entity. it has some columns in terms of token, and it also has ManyToOne relation with USER table.
 * @author Behrouz Takhti
 * @version 0.0.1-SNAPSHOT
 * @since 2024-01-14
 */
@Entity
@Table(name = "TOKENS")
@SequenceGenerator(name = "tokenSequence", sequenceName = "TOKENS_SEQ", initialValue = 1, allocationSize = 1)
public class Tokens {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tokenSequence")
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "TOKEN", unique = true)
    private String token;

    @Enumerated(EnumType.STRING)
    private TokenType tokenType = TokenType.BEARER;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TOKEN_USR_ID", nullable = false)
    private Users user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public TokenType getTokenType() {
        return tokenType;
    }

    public void setTokenType(TokenType tokenType) {
        this.tokenType = tokenType;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }
}
