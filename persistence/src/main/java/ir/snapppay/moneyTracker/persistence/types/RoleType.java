package ir.snapppay.moneyTracker.persistence.types;


/**
 * This enum is used for determine the ROLE of the user.
 * @author Behrouz Takhti
 * @version 0.0.1-SNAPSHOT
 * @since 2024-01-14
 * @see ir.snapppay.moneyTracker.persistence.domain.Users
 */
public enum RoleType {

    USER,
    ADMIN;
}
