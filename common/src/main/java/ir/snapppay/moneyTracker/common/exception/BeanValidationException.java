package ir.snapppay.moneyTracker.common.exception;

import org.springframework.validation.Errors;

/**
 * This is a type of RuntimeException that used in a situation that BeanValidationException has occurred.
 * @author Behrouz Takhti
 * @version 0.0.1-SNAPSHOT
 * @since 2024-01-14
 * @see GlobalExceptionHandler
 */
public class BeanValidationException extends RuntimeException{

    private Errors errors;

    public BeanValidationException(String message, Errors errors) {
        super(message);
        this.errors = errors;
    }

    public Errors getErrors() {
        return errors;
    }

    public void setErrors(Errors errors) {
        this.errors = errors;
    }
}
