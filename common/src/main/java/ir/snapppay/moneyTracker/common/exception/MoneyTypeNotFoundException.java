package ir.snapppay.moneyTracker.common.exception;

/**
 * This is a type of RuntimeException that used in a situation that money affair type not found.
 * @author Behrouz Takhti
 * @version 0.0.3-SNAPSHOT
 * @since 2024-01-15
 * @see GlobalExceptionHandler
 */
public class MoneyTypeNotFoundException extends RuntimeException{

    public MoneyTypeNotFoundException() {
    }

    public MoneyTypeNotFoundException(String message) {
        super(message);
    }

    public MoneyTypeNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
