package ir.snapppay.moneyTracker.common.aspect;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.*;


/**
 * This is Enum validator that we can use it in requestDTO in order to validation a special enumerated field.
 * @author Behrouz Takhti
 * @version 0.0.2-SNAPSHOT
 * @since 2024-01-15
 * @see EnumValidatorImpl
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE})
@Constraint(validatedBy = EnumValidatorImpl.class)
public @interface EnumValidator {

    Class<? extends Enum<?>> enumClazz();
    String message() default "مقادیر نامعتبر می باشد";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
