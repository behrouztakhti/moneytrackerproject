package ir.snapppay.moneyTracker.common.aspect;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * This is Enum validator implementation that we can use it in requestDTO in order to validation a special enumerated field.
 * @author Behrouz Takhti
 * @version 0.0.2-SNAPSHOT
 * @since 2024-01-15
 */
public class EnumValidatorImpl implements ConstraintValidator<EnumValidator, String> {


    List<String> ls = new ArrayList<>();
    @Override
    public void initialize(EnumValidator enumValidator) {
        Class<? extends Enum<?>> enumClazz = enumValidator.enumClazz();
        Enum[] enumValues = enumClazz.getEnumConstants();
        for (Enum e : enumValues){
            ls.add(e.toString().toUpperCase());
        }
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (Objects.isNull(s)){
            return true;
        }
        if (ls.contains(s.toUpperCase())){
            return true;
        }else {
            return false;
        }
    }
}
