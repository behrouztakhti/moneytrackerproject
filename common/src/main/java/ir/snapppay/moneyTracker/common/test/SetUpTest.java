package ir.snapppay.moneyTracker.common.test;


import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.*;
import org.springframework.context.annotation.Profile;


/**
 * This is utility class for test environment.
 * @author Behrouz Takhti
 * @version 0.0.3-SNAPSHOT
 * @since 2024-01-15
 */
@Profile("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SetUpTest {

    public ObjectWriter objectWriter;

    @BeforeAll
    public void beforeAll(){
        objectWriter = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false)
                .registerModule(new JavaTimeModule()).writer().withDefaultPrettyPrinter();
        System.out.println("***** before all tests *****");
    }

    @AfterAll
    public void afterAll(){
        System.out.println("***** after all tests *****");
    }

    @BeforeEach
    public void beforeEach(TestInfo testInfo){
        System.out.println("***** start to running Test - " +testInfo.getDisplayName() + " *****");
    }

    @AfterEach
    public void afterEach(TestInfo testInfo){
        System.out.println("***** end of running Test - " +testInfo.getDisplayName() + " *****");
    }

}
