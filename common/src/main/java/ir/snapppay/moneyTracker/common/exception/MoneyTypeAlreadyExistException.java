package ir.snapppay.moneyTracker.common.exception;

/**
 * This is a type of RuntimeException that used in a situation that money affair type already exist.
 * @author Behrouz Takhti
 * @version 0.0.2-SNAPSHOT
 * @since 2024-01-15
 * @see GlobalExceptionHandler
 */
public class MoneyTypeAlreadyExistException extends RuntimeException{

    public MoneyTypeAlreadyExistException() {
    }

    public MoneyTypeAlreadyExistException(String message) {
        super(message);
    }

    public MoneyTypeAlreadyExistException(String message, Throwable cause) {
        super(message, cause);
    }
}
