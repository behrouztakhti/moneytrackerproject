package ir.snapppay.moneyTracker.common.utils;

import ir.snapppay.moneyTracker.common.dto.GenericResponse;
import ir.snapppay.moneyTracker.common.enums.ResponseStatusEnum;
import ir.snapppay.moneyTracker.common.exception.BeanValidationException;
import ir.snapppay.moneyTracker.common.exception.BeanValidationGenericResponse;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;


/**
 * This is utility class that has some utilities.
 * @author Behrouz Takhti
 * @version 0.0.1-SNAPSHOT
 * @since 2024-01-14
 */
public abstract class GeneralUtils {

    public static GenericResponse failedResponse(){
        return new GenericResponse(HttpStatus.BAD_REQUEST.value(), ResponseStatusEnum.FAILED.name(), ResponseStatusEnum.FAILED.toMessage());
    }

    public static BeanValidationGenericResponse beanErrorResponse(){
        return new BeanValidationGenericResponse(HttpStatus.BAD_REQUEST.value(), ResponseStatusEnum.FAILED.name(), ResponseStatusEnum.FAILED.toMessage());
    }

    public static void checkBindingResult(BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            BeanValidationException beanValidationException = new BeanValidationException("مقادیر ارسالی نامعتبر می باشد", bindingResult);
            throw beanValidationException;
        }
    }


}
