package ir.snapppay.moneyTracker.common.exception;


/**
 * This is used in  application BeanValidation exception.
 * @author Behrouz Takhti
 * @version 0.0.1-SNAPSHOT
 * @since 2024-01-14
 * @see BeanValidationGenericResponse
 */
public class FieldValidationError {

    private String field;
    private String message;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
