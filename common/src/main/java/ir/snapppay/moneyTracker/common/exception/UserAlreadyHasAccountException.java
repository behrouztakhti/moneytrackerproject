package ir.snapppay.moneyTracker.common.exception;

/**
 * This is a type of RuntimeException that used in a situation that User already has account then they can not register again.
 * @author Behrouz Takhti
 * @version 0.0.1-SNAPSHOT
 * @since 2024-01-14
 * @see GlobalExceptionHandler
 */
public class UserAlreadyHasAccountException extends RuntimeException{

    public UserAlreadyHasAccountException() {
    }

    public UserAlreadyHasAccountException(String message) {
        super(message);
    }

    public UserAlreadyHasAccountException(String message, Throwable cause) {
        super(message, cause);
    }
}
