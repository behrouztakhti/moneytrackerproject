package ir.snapppay.moneyTracker.common.enums;

/**
 * This enum is used for determine the response code.
 * @author Behrouz Takhti
 * @version 0.0.1-SNAPSHOT
 * @since 2024-01-14
 * @see ir.snapppay.moneyTracker.common.utils.GeneralUtils
 */
public enum ResponseStatusEnum {

    FAILED((byte) 0, "عملیات دارای خطا می باشد"),
    SUCCESS((byte) 1, "عملیات با موفقیت انجام شد");

    private byte value;
    private String message;

    private ResponseStatusEnum(byte value, String message){
        this.value = value;
        this.message = message;
    }

    public byte toValue(){
        return this.value;
    }
    public String toMessage(){
        return this.message;
    }
}
