package ir.snapppay.moneyTracker.common.exception;

import ir.snapppay.moneyTracker.common.dto.GenericResponse;

import java.util.List;


/**
 * This is a generic response of application BeanValidation exception.
 * @author Behrouz Takhti
 * @version 0.0.1-SNAPSHOT
 * @since 2024-01-14
 */
public class BeanValidationGenericResponse extends GenericResponse {

    private List<FieldValidationError> fieldValidationErrors;

    public BeanValidationGenericResponse(int code, String status, String message) {
        super(code, status, message);
    }

    public List<FieldValidationError> getFieldValidationErrors() {
        return fieldValidationErrors;
    }

    public void setFieldValidationErrors(List<FieldValidationError> fieldValidationErrors) {
        this.fieldValidationErrors = fieldValidationErrors;
    }
}
