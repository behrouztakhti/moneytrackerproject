package ir.snapppay.moneyTracker.common.exception;

import ir.snapppay.moneyTracker.common.dto.GenericResponse;
import ir.snapppay.moneyTracker.common.utils.GeneralUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is responsible for handling exceptions across the whole application in one global handling component.
 * @author Behrouz Takhti
 * @version 0.0.1-SNAPSHOT
 * @since 2024-01-14
 */
@ControllerAdvice
@Component
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {


    /**
     * This ExceptionHandler catch any exception that has occurred in application which has not written any special method for it.
     * @param  exp Exception.
     * @param  webRequest WebRequest.
     */
    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> exception(Exception exp, WebRequest webRequest){
        GenericResponse response = GeneralUtils.failedResponse();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return handleExceptionInternal(exp, response, httpHeaders, HttpStatus.BAD_REQUEST, webRequest);
    }


    /**
     * This ExceptionHandler catch BeanValidationException exception and translate it to application special error response.
     * @param  exp BeanValidationException.
     * @param  webRequest WebRequest.
     */
    @ExceptionHandler({BeanValidationException.class})
    public ResponseEntity<Object> beanValidationException(BeanValidationException exp, WebRequest webRequest){
        List<FieldValidationError> fieldValidationErrors = new ArrayList<>();
        if (exp.getErrors().getFieldErrorCount() > 0){
            List<FieldError> fieldErrors = exp.getErrors().getFieldErrors();
            fieldValidationErrors = fieldErrors.stream().map(item -> {
                FieldValidationError dt = new FieldValidationError();
                dt.setField(item.getField());
                dt.setMessage(item.getDefaultMessage());
                return dt;
            }).toList();
        }
        BeanValidationGenericResponse errorResponse = GeneralUtils.beanErrorResponse();
        errorResponse.setMessage(exp.getMessage());
        errorResponse.setFieldValidationErrors(fieldValidationErrors);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return handleExceptionInternal(exp, errorResponse, httpHeaders, HttpStatus.BAD_REQUEST, webRequest);
    }

    /**
     * This ExceptionHandler catch UserAlreadyHasAccountException exception and translate it to application special error response.
     * @param  exp BeanValidationException.
     * @param  webRequest WebRequest.
     */
    @ExceptionHandler({UserAlreadyHasAccountException.class})
    public ResponseEntity<Object> userAlreadyHasAccountException(UserAlreadyHasAccountException exp, WebRequest webRequest){
        return this.handleExp(exp, webRequest);
    }

    /**
     * This ExceptionHandler catch MoneyTypeAlreadyExistException exception and translate it to application special error response.
     * @param  exp BeanValidationException.
     * @param  webRequest WebRequest.
     */
    @ExceptionHandler({MoneyTypeAlreadyExistException.class})
    public ResponseEntity<Object> moneyTypeAlreadyExistException(MoneyTypeAlreadyExistException exp, WebRequest webRequest){
        return this.handleExp(exp, webRequest);
    }

    /**
     * This ExceptionHandler catch MoneyTypeNotFoundException exception and translate it to application special error response.
     * @param  exp BeanValidationException.
     * @param  webRequest WebRequest.
     */
    @ExceptionHandler({MoneyTypeNotFoundException.class})
    public ResponseEntity<Object> moneyTypeNotFoundException(MoneyTypeNotFoundException exp, WebRequest webRequest){
        return this.handleExp(exp, webRequest);
    }

    /**
     * This ExceptionHandler catch GeneralException exception and translate it to application special error response.
     * @param  exp BeanValidationException.
     * @param  webRequest WebRequest.
     */
    @ExceptionHandler({GeneralException.class})
    public ResponseEntity<Object> generalException(GeneralException exp, WebRequest webRequest){
        return this.handleExp(exp, webRequest);
    }
    private ResponseEntity<Object> handleExp(Exception exp, WebRequest webRequest){
        GenericResponse response = new GenericResponse(HttpStatus.BAD_REQUEST.value(), exp.getMessage());
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return handleExceptionInternal(exp, response, httpHeaders, HttpStatus.BAD_REQUEST, webRequest);
    }




}
