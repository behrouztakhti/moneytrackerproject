# moneyTracker
- This is a project that I've chosen for snappPay code challenging.
- This project has some functionality that enables us to categorize our daily expenses.
By the use of this project we are able to add our daily expenses and incomes and manage our financial daily affairs.

# About the Project
This service is a simple daily financial affairs REST Services.
It uses an in-memory database(H2) to store the data.You can also do with a relational database like Oracle or PostgreSQL.
You can call All endpoints on **PORT** **9999**.

# Requirements
- JDK 17
- Maven 3

# How to build
You can build the project by running ```mvn clean package```. 
The application will be packaged as a jar file on the target folder of **launcher** module.

# How to run
There are several ways to run spring boot application.
The easiest way is that you can run the produced jar file(ar file on the target folder of launcher module)
by ```java -jar``` command.

Once the application runs you should see something like this:
```
Tomcat started on port(s): 9999 (http).
Started LaunchApplication in 12.816 seconds.
moneyTracker application started successfully !
```
# To view swagger API docs
Run the server and browse to http://localhost:9999/swagger-ui/index.html

# To view your H2 in-memory database
To view and query the database you can browse to
http://localhost:9999/h2-console
Make sure you disable this in production environment.