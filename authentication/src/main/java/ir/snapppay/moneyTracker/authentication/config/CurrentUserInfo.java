package ir.snapppay.moneyTracker.authentication.config;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;


/**
 * This class is responsible for providing the application current user.
 * @author Behrouz Takhti
 * @version 0.0.2-SNAPSHOT
 * @since 2024-01-15
 */
public interface CurrentUserInfo {

    default SystemUser getCurrentUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || !authentication.isAuthenticated() || authentication instanceof AnonymousAuthenticationToken){
            return null;
        }
        return (SystemUser)authentication.getPrincipal();
    }
}
