package ir.snapppay.moneyTracker.authentication.config;

import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

/**
 * This class uses jpa auditing for audit the intended variables.
 * @author Behrouz Takhti
 * @version 0.0.2-SNAPSHOT
 * @since 2024-01-15
 */
public class ApplicationAuditAware implements AuditorAware<Long>, CurrentUserInfo {

    @Override
    public Optional<Long> getCurrentAuditor() {
        SystemUser systemUser = getCurrentUser();
        return Optional.ofNullable(systemUser.getId());
    }
}
