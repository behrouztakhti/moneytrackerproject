package ir.snapppay.moneyTracker.authentication.rest;

import io.swagger.v3.oas.annotations.Operation;
import ir.snapppay.moneyTracker.authentication.service.AuthenticationService;
import ir.snapppay.moneyTracker.authentication.dto.UserSignupRequestDTO;
import ir.snapppay.moneyTracker.authentication.dto.UserLoginRequestDTO;
import ir.snapppay.moneyTracker.common.dto.GenericResponse;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * This controller is responsible for user authentication process such as signup, login and ...
 * @author Behrouz Takhti
 * @version 0.0.1-SNAPSHOT
 * @since 2024-01-14
 */
@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    private AuthenticationService authenticationSrv;

    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationSrv = authenticationService;
    }



    /**
     * this method is responsible for user signup process.
     * @param  request HttpServletRequest.
     * @param  signupRequestDTO A request DTO.
     * @param  bindingResult for bean validation.
     * @return GenericResponse a generic response of application.
     */
    @Operation(summary = "ثبت نام کاربر")
    @PostMapping(value ="/signup")
    public GenericResponse signup(HttpServletRequest request, @RequestBody @Validated UserSignupRequestDTO signupRequestDTO, BindingResult bindingResult) throws Exception{
        return authenticationSrv.signup(request, signupRequestDTO, bindingResult);
    }


    /**
     * this method is responsible for user login process.
     * @param  request HttpServletRequest.
     * @param  loginRequestDTO A request DTO.
     * @param  bindingResult for bean validation.
     * @return GenericResponse a generic response of application.
     */
    @Operation(summary = "ورود کاربر")
    @PostMapping(value ="/login")
    public GenericResponse login(HttpServletRequest request, @RequestBody @Validated UserLoginRequestDTO loginRequestDTO, BindingResult bindingResult) throws Exception{
        return authenticationSrv.login(request, loginRequestDTO, bindingResult);
    }

}
