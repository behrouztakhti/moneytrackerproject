package ir.snapppay.moneyTracker.authentication.config;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import java.io.IOException;


/**
 * AuthenticationEntryPoint handles authentication exception.
 * I've customized it.
 * @author Behrouz Takhti
 * @version 0.0.1-SNAPSHOT
 * @since 2024-01-14
 * @see SecurityConfig
 */
@Component
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {


    /**
     * this method handles authentication exception.
     * @param  request HttpServletRequest.
     * @param  response HttpServletResponse.
     * @param  authException AuthenticationException.
     * @see CustomAuthenticationEntryPoint
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Authentication failed !!!");
    }
}
