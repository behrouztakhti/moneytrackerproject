package ir.snapppay.moneyTracker.authentication.service;

import ir.snapppay.moneyTracker.authentication.config.jwt.JwtService;
import ir.snapppay.moneyTracker.authentication.dto.UserLoginRequestDTO;
import ir.snapppay.moneyTracker.authentication.dto.UserSignupRequestDTO;
import ir.snapppay.moneyTracker.common.dto.GenericResponse;
import ir.snapppay.moneyTracker.common.enums.ResponseStatusEnum;
import ir.snapppay.moneyTracker.common.exception.UserAlreadyHasAccountException;
import ir.snapppay.moneyTracker.common.utils.GeneralUtils;
import ir.snapppay.moneyTracker.persistence.domain.Tokens;
import ir.snapppay.moneyTracker.persistence.domain.Users;
import ir.snapppay.moneyTracker.persistence.repository.TokenRepository;
import ir.snapppay.moneyTracker.persistence.repository.UserRepository;
import ir.snapppay.moneyTracker.persistence.types.RoleType;
import ir.snapppay.moneyTracker.persistence.types.TokenType;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import java.util.Optional;


/**
 * This class has implemented the AuthenticationService.
 * It is responsible for implementing the user authentication process such as signup, login and ...
 * @author Behrouz Takhti
 * @version 0.0.1-SNAPSHOT
 * @since 2024-01-14
 * @see AuthenticationService
 */
@Service
public class AuthenticationServiceImpl implements AuthenticationService{

    private PasswordEncoder passwordEncoder;
    private UserRepository userRepo;
    private JwtService jwtSrv;
    private TokenRepository tokenRepo;
    private AuthenticationManager authenticationManager;

    public AuthenticationServiceImpl(PasswordEncoder passwordEncoder, UserRepository userRepo,
                                     JwtService jwtSrv, TokenRepository tokenRepo, AuthenticationManager authenticationManager) {
        this.passwordEncoder = passwordEncoder;
        this.userRepo = userRepo;
        this.jwtSrv = jwtSrv;
        this.tokenRepo = tokenRepo;
        this.authenticationManager = authenticationManager;
    }


    /**
     * this method is responsible for user signup process.
     * @param  request HttpServletRequest.
     * @param  signupRequestDTO A request DTO.
     * @param  bindingResult for bean validation.
     * @return GenericResponse a generic response of application.
     */
    @Override
    public GenericResponse signup(HttpServletRequest request, UserSignupRequestDTO signupRequestDTO, BindingResult bindingResult) throws Exception{
        GeneralUtils.checkBindingResult(bindingResult);
        Optional<Users> userInDB = userRepo.findByUsername(signupRequestDTO.getUsername());
        userInDB.ifPresent(users -> {
            throw new UserAlreadyHasAccountException("User Already Has Account");
        });
        var userForSave = new Users();
        BeanUtils.copyProperties(signupRequestDTO, userForSave, "password");
        userForSave.setPassword(passwordEncoder.encode(signupRequestDTO.getPassword()));
        userForSave.setRole(RoleType.USER);
        var savedUser = userRepo.save(userForSave);
        var jwtToken = jwtSrv.generateToken(userForSave);
        saveUserToken(savedUser, jwtToken);
        return new GenericResponse(HttpStatus.OK.value(), ResponseStatusEnum.SUCCESS.name(), ResponseStatusEnum.SUCCESS.toMessage(), jwtToken);
    }


    /**
     * this method is responsible for user login process.
     * @param  request HttpServletRequest.
     * @param  loginRequestDTO A request DTO.
     * @param  bindingResult for bean validation.
     * @return GenericResponse a generic response of application.
     */
    @Override
    public GenericResponse login(HttpServletRequest request, UserLoginRequestDTO loginRequestDTO, BindingResult bindingResult) throws Exception {
        GeneralUtils.checkBindingResult(bindingResult);
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequestDTO.getUsername(), loginRequestDTO.getPassword()));
        var user = userRepo.findByUsername(loginRequestDTO.getUsername()).orElseThrow();
        tokenRepo.deleteAllByUserId(user.getId());
        var jwtToken = jwtSrv.generateToken(user);
        saveUserToken(user, jwtToken);
        return new GenericResponse(HttpStatus.OK.value(), ResponseStatusEnum.SUCCESS.name(), ResponseStatusEnum.SUCCESS.toMessage(), jwtToken);
    }



    /**
     * this private method is called in  both signup and login method and is responsible for save UserToken.
     * @param  user HttpServletRequest.
     * @param  jwtToken A request DTO.
     */
    private void saveUserToken(Users user, String jwtToken){
        var tokenForSave = new Tokens();
        tokenForSave.setUser(user);
        tokenForSave.setToken(jwtToken);
        tokenForSave.setTokenType(TokenType.BEARER);
        tokenRepo.save(tokenForSave);
    }
}
