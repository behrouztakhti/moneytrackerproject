package ir.snapppay.moneyTracker.authentication.config;

import ir.snapppay.moneyTracker.persistence.domain.Users;
import ir.snapppay.moneyTracker.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * UserDetailsService is used by DaoAuthenticationProvider for retrieving a username and other attributes for authenticating with username.
 * I've customized it and bind it to UserRepository.
 * @author Behrouz Takhti
 * @version 0.0.1-SNAPSHOT
 * @since 2024-01-14
 * @see ApplicationConfig
 */
@Service
@Transactional
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepo;




    /**
     * this method gives username and fetch user(if exists) from database and returns UserDetails.
     * @param  username the username of user that we give it from client.
     * @return systemUser
     * @see SecurityConfig
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            Users userInDB;
            Optional<Users> retVal = userRepo.findByUsername(username);
            if (retVal.isPresent()){
                userInDB = retVal.get();
            }else {
                throw new UsernameNotFoundException("user not found !");
            }
            boolean enabled = true;
            boolean accountNonExpired = true;
            boolean credentialsNonExpired = true;
            boolean accountNonLocked = true;
            SystemUser systemUser = new SystemUser(
                    username,
                    userInDB.getPassword(),
                    enabled,
                    accountNonExpired,
                    credentialsNonExpired,
                    accountNonLocked,
                    getAuthorities(userInDB));
            systemUser.setId(userInDB.getId());
            systemUser.setName(userInDB.getName());
            systemUser.setFamily(userInDB.getFamily());
            systemUser.setFullName(userInDB.getName().concat(" ").concat(userInDB.getFamily()));
            systemUser.setUsername(userInDB.getUsername());
            return systemUser;
        }catch (UsernameNotFoundException e){
            throw new UsernameNotFoundException("error", e);
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    private static Set<GrantedAuthority> getAuthorities (Users user) {
        Set<GrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_" + user.getRole().name()));
        return authorities;
    }

}
