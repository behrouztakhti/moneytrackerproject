package ir.snapppay.moneyTracker.authentication.config;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;
import java.io.IOException;


/**
 * AccessDeniedHandler handles authorisation exception.
 * I've customized it.
 * @author Behrouz Takhti
 * @version 0.0.1-SNAPSHOT
 * @since 2024-01-14
 * @see SecurityConfig
 */
@Component
public class CustomAccessDeniedHandler implements AccessDeniedHandler {

    /**
     * this method handles authorisation exception.
     * @param  request HttpServletRequest.
     * @param  response HttpServletResponse.
     * @param  accessDeniedException AccessDeniedException.
     * @see CustomAccessDeniedHandler
     */
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        response.sendError(HttpServletResponse.SC_FORBIDDEN, "inadequate authorization !");
    }
}
