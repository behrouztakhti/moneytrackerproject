package ir.snapppay.moneyTracker.authentication.service;

import ir.snapppay.moneyTracker.authentication.dto.UserLoginRequestDTO;
import ir.snapppay.moneyTracker.authentication.dto.UserSignupRequestDTO;
import ir.snapppay.moneyTracker.common.dto.GenericResponse;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.validation.BindingResult;


/**
 * This interface is responsible for user authentication process such as signup, login and ...
 * @author Behrouz Takhti
 * @version 0.0.1-SNAPSHOT
 * @since 2024-01-14
 * @see ir.snapppay.moneyTracker.authentication.rest.AuthenticationController
 */
public interface AuthenticationService {


    /**
     * this method is responsible for user signup process.
     * @param  request HttpServletRequest.
     * @param  signupRequestDTO A request DTO.
     * @param  bindingResult for bean validation.
     * @return GenericResponse a generic response of application.
     */
    GenericResponse signup(HttpServletRequest request, UserSignupRequestDTO signupRequestDTO, BindingResult bindingResult) throws Exception;


    /**
     * this method is responsible for user login process.
     * @param  request HttpServletRequest.
     * @param  loginRequestDTO A request DTO.
     * @param  bindingResult for bean validation.
     * @return GenericResponse a generic response of application.
     */
    GenericResponse login(HttpServletRequest request, UserLoginRequestDTO loginRequestDTO, BindingResult bindingResult) throws Exception;
}
