package ir.snapppay.moneyTracker.authentication.config.jwt;

import ir.snapppay.moneyTracker.persistence.domain.Users;


/**
 * This interface has some methods in terms of jwtToken.
 * I've customized it.
 * @author Behrouz Takhti
 * @version 0.0.1-SNAPSHOT
 * @since 2024-01-14
 * @see JwtAuthenticationFilter
 */
public interface JwtService {

    String extractUsername(String token);
    Boolean isTokenValid(String token);
    String generateToken(Users user);
}
