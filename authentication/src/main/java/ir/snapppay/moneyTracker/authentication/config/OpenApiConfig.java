package ir.snapppay.moneyTracker.authentication.config;


import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.servers.Server;
import org.springframework.context.annotation.Configuration;

/**
 * This class is Configuration class in which we are able to create spring doc and swagger ui in order to documentation.
 * @author Behrouz Takhti
 * @version 0.0.3-SNAPSHOT
 * @since 2024-01-15
 */

@OpenAPIDefinition(
        info = @Info(
                title = "راهنمای سرویسهای سامانه (دخل و خرج روزانه)",
                description = "در اینجا شما میتوانید با سرویسهای سامانه (دخل وخرج روزانه) آشنا و از آنها بهره مند شوید",
                version = "0.0.3",
                contact = @Contact(
                        name = "snapppay",
                        url = "https://snapppay.ir/",
                        email = "behrouz.takhti@gmail.com"
                )
        ),
        servers = {
                @Server(url = "http://localhost:9999", description = "محیط توسعه")
        },
        security = {
                @SecurityRequirement(name = "BearerAuth")
        }
)
@SecurityScheme(
        name = "BearerAuth",
        type = SecuritySchemeType.HTTP,
        bearerFormat = "JWT",
        description = "JWT Authentication",
        scheme = "Bearer",
        in = SecuritySchemeIn.HEADER
)
@Configuration
public class OpenApiConfig {
}
