package ir.snapppay.moneyTracker.authentication.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;


/**
 * This class is used in User-signup process as request DTO;
 * @author Behrouz Takhti
 * @version 0.0.1-SNAPSHOT
 * @since 2024-01-14
 * @see ir.snapppay.moneyTracker.authentication.rest.AuthenticationController
 */
public class UserSignupRequestDTO {

    @Schema(description = "نام کاربر", example = "بهروز")
    @NotBlank(message = "NotBlank.name")
    private String name;

    @Schema(description = "نام خانوادگی کاربر", example = "تختی")
    @NotBlank(message = "NotBlank.family")
    private String family;

    @Schema(description = "نام کاربری", example = "firstUser")
    @NotBlank(message = "NotBlank.username")
    private String username;

    @Schema(description = "رمز عبور", example = "qaz@123")
    @NotBlank(message = "NotBlank.password")
    private String password;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
