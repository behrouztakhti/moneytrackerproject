package ir.snapppay.moneyTracker.authentication.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

/**
 * This class is used in User-login process as request DTO;
 * @author Behrouz Takhti
 * @version 0.0.1-SNAPSHOT
 * @since 2024-01-14
 * @see ir.snapppay.moneyTracker.authentication.rest.AuthenticationController
 */
public class UserLoginRequestDTO {

    @Schema(description = "نام کاربری", example = "firstUser")
    @NotBlank(message = "NotBlank.username")
    private String username;

    @Schema(description = "رمز عبور", example = "qaz@123")
    @NotBlank(message = "NotBlank.password")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
