package ir.snapppay.moneyTracker.authentication.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import ir.snapppay.moneyTracker.authentication.config.*;
import ir.snapppay.moneyTracker.authentication.config.jwt.JwtAuthenticationFilter;
import ir.snapppay.moneyTracker.authentication.dto.UserLoginRequestDTO;
import ir.snapppay.moneyTracker.authentication.dto.UserSignupRequestDTO;
import ir.snapppay.moneyTracker.authentication.service.AuthenticationService;
import ir.snapppay.moneyTracker.common.test.SetUpTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles(value = "test")
@WebMvcTest(controllers = AuthenticationController.class )
@ContextConfiguration(classes = {AuthenticationController.class})
@Import({SecurityConfig.class})
public class AuthenticationControllerTest  extends SetUpTest{

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    AuthenticationService authenticationSrv;
    @MockBean
    JwtAuthenticationFilter jwtAuthFilter;
    @MockBean
    LogoutHandler logoutHandler;
    @MockBean
    CustomAuthenticationEntryPoint customAuthenticationEntryPoint;
    @MockBean
    CustomAccessDeniedHandler accessDeniedHandler;
    @MockBean
    AuthenticationProvider authenticationProvider;

    ///////////////////////// --------- /auth/signup ---------  ////////////////////
    @Test
    @DisplayName("signup_whenValidInput_thenReturns200")
    public void signup_whenValidInput_thenReturns200() throws Exception{
        var dto = new UserSignupRequestDTO();
        dto.setName("بهروز");
        dto.setFamily("تختی");
        dto.setUsername("firstUser");
        dto.setPassword("qaz@123");
        mockMvc.perform(
                        post("/auth/signup")
                                .with(csrf())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(new ObjectMapper().writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isOk());
    }
    ///////////////////////// --------- /auth/login ---------  ////////////////////
    @Test
    @DisplayName("login_whenValidInput_thenReturns200")
    public void login_whenValidInput_thenReturns200() throws Exception{
        var dto = new UserLoginRequestDTO();
        dto.setUsername("firstUser");
        dto.setPassword("qaz@123");
        mockMvc.perform(
                        post("/auth/login")
                                .with(csrf())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(new ObjectMapper().writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isOk());
    }



}
