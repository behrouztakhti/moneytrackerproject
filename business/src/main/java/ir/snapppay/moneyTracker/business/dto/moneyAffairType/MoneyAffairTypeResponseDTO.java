package ir.snapppay.moneyTracker.business.dto.moneyAffairType;


import com.fasterxml.jackson.annotation.JsonInclude;
import ir.snapppay.moneyTracker.business.rest.moneyAffairType.MoneyAffairTypeController;

/**
 * This class is used in daily report per type process as request DTO;
 * @author Behrouz Takhti
 * @version 0.0.-SNAPSHOT
 * @since 2024-01-15
 * @see MoneyAffairTypeController
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public record MoneyAffairTypeResponseDTO(Long id, String name, String persianName, String type, String description) {
}
