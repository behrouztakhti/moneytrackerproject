package ir.snapppay.moneyTracker.business.dto.moneyAffairType;

import io.swagger.v3.oas.annotations.media.Schema;
import ir.snapppay.moneyTracker.business.rest.moneyAffairType.MoneyAffairTypeController;
import ir.snapppay.moneyTracker.common.aspect.EnumValidator;
import ir.snapppay.moneyTracker.persistence.types.MoneyTypeEnum;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

/**
 * This class is used in User-add money affair type process as request DTO;
 * @author Behrouz Takhti
 * @version 0.0.2-SNAPSHOT
 * @since 2024-01-15
 * @see MoneyAffairTypeController
 */
public class MoneyAffairTypeRequestDTO {

    @Schema(description = "نام هزینه یا درآمد", example = "breakfast")
    @NotBlank(message = "NotBlank.name")
    private String name;

    @Schema(description = "نام فارسی هزینه یا درآمد", example = "صبحانه")
    @NotBlank(message = "NotBlank.persianName")
    private String persianName;

    @Schema(description = "نوع", example = "EXPENSE(اگر از نوع هزینه باشد) - INCOME(اگر از نوع درآمد باشد)")
    @NotNull(message = "NotNull.type")
    @EnumValidator(enumClazz = MoneyTypeEnum.class, message = "MoneyAffairType.invalid")
    private String type;

    @Schema(description = "توضیحات", example = "توضیحات")
    @NotBlank(message = "NotBlank.description")
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPersianName() {
        return persianName;
    }

    public void setPersianName(String persianName) {
        this.persianName = persianName;
    }
}
