package ir.snapppay.moneyTracker.business.service.moneyAffairType;

import ir.snapppay.moneyTracker.authentication.config.CurrentUserInfo;
import ir.snapppay.moneyTracker.business.dto.moneyAffairType.MoneyAffairTypeRequestDTO;
import ir.snapppay.moneyTracker.business.rest.moneyAffairType.MoneyAffairTypeController;
import ir.snapppay.moneyTracker.common.dto.GenericResponse;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.validation.BindingResult;


/**
 * This interface is responsible for money affair type process such as add type and ...
 * @author Behrouz Takhti
 * @version 0.0.2-SNAPSHOT
 * @since 2024-01-15
 * @see MoneyAffairTypeController
 */
public interface MoneyAffairTypeService  extends CurrentUserInfo {

    /**
     * this method is responsible for add money affair type process.
     * @param  request HttpServletRequest.
     * @param  moneyAffairTypeRequestDTO A request DTO.
     * @param  bindingResult for bean validation.
     * @return GenericResponse a generic response of application.
     */
    GenericResponse addType(HttpServletRequest request, MoneyAffairTypeRequestDTO moneyAffairTypeRequestDTO, BindingResult bindingResult) throws Exception;
}
