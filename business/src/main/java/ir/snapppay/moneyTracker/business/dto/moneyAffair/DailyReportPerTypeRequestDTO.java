package ir.snapppay.moneyTracker.business.dto.moneyAffair;

import io.swagger.v3.oas.annotations.media.Schema;
import ir.snapppay.moneyTracker.business.rest.moneyAffairType.MoneyAffairTypeController;
import jakarta.validation.constraints.NotNull;

/**
 * This class is used in daily report per type process as request DTO;
 * @author Behrouz Takhti
 * @version 0.0.-SNAPSHOT
 * @since 2024-01-15
 * @see MoneyAffairTypeController
 */
public class DailyReportPerTypeRequestDTO  extends DailyReportRequestDTO{


    @Schema(description = "شناسه نوع هزینه یا درآمد", example = "1")
    @NotNull(message = "NotNull.moneyAffairTypeID")
    private Long moneyAffairTypeID;

    public Long getMoneyAffairTypeID() {
        return moneyAffairTypeID;
    }

    public void setMoneyAffairTypeID(Long moneyAffairTypeID) {
        this.moneyAffairTypeID = moneyAffairTypeID;
    }
}
