package ir.snapppay.moneyTracker.business.service.moneyAffair;

import ir.snapppay.moneyTracker.authentication.config.CurrentUserInfo;
import ir.snapppay.moneyTracker.business.dto.moneyAffair.DailyReportPerTypeRequestDTO;
import ir.snapppay.moneyTracker.business.dto.moneyAffair.DailyReportRequestDTO;
import ir.snapppay.moneyTracker.business.dto.moneyAffair.MoneyAffairRequestDTO;
import ir.snapppay.moneyTracker.business.rest.moneyAffairType.MoneyAffairTypeController;
import ir.snapppay.moneyTracker.common.dto.GenericResponse;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.validation.BindingResult;


/**
 * This interface is responsible for money affair process such as add and ...
 * @author Behrouz Takhti
 * @version 0.0.3-SNAPSHOT
 * @since 2024-01-15
 * @see MoneyAffairTypeController
 */
public interface MoneyAffairsService extends CurrentUserInfo {

    /**
     * this method is responsible for add money affair process.
     * @param  request HttpServletRequest.
     * @param  moneyAffairRequestDTO A request DTO.
     * @param  bindingResult for bean validation.
     * @return GenericResponse a generic response of application.
     */
    GenericResponse addAffair(HttpServletRequest request, MoneyAffairRequestDTO moneyAffairRequestDTO, BindingResult bindingResult) throws Exception;


    /**
     * this method is responsible for add daily report per type process.
     * @param  request HttpServletRequest.
     * @param  dailyReportPerTypeRequestDTO A request DTO.
     * @param  bindingResult for bean validation.
     * @return GenericResponse a generic response of application.
     */
    GenericResponse dailyReportPerType(HttpServletRequest request, DailyReportPerTypeRequestDTO dailyReportPerTypeRequestDTO, BindingResult bindingResult) throws Exception;


    /**
     * this method is responsible for add daily report process. this gives us accounting per day.
     * @param  request HttpServletRequest.
     * @param  dailyReportRequestDTO A request DTO.
     * @param  bindingResult for bean validation.
     * @return GenericResponse a generic response of application.
     */
    GenericResponse dailyReport(HttpServletRequest request, DailyReportRequestDTO dailyReportRequestDTO, BindingResult bindingResult) throws Exception;
}
