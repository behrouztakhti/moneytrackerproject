package ir.snapppay.moneyTracker.business.rest.moneyAffair;

import io.swagger.v3.oas.annotations.Operation;
import ir.snapppay.moneyTracker.business.dto.moneyAffair.DailyReportPerTypeRequestDTO;
import ir.snapppay.moneyTracker.business.dto.moneyAffair.DailyReportRequestDTO;
import ir.snapppay.moneyTracker.business.dto.moneyAffair.MoneyAffairRequestDTO;
import ir.snapppay.moneyTracker.business.service.moneyAffair.MoneyAffairsService;
import ir.snapppay.moneyTracker.common.dto.GenericResponse;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * This controller is responsible for money affair type process such as add type and ...
 * @author Behrouz Takhti
 * @version 0.0.3-SNAPSHOT
 * @since 2024-01-15
 */
@RestController
@RequestMapping(value = "/money-affairs")
public class MoneyAffairsController {

    private MoneyAffairsService moneyAffairsSrv;

    public MoneyAffairsController(MoneyAffairsService moneyAffairsSrv) {
        this.moneyAffairsSrv = moneyAffairsSrv;
    }

    public void setMoneyAffairsSrv(MoneyAffairsService moneyAffairsSrv) {
        this.moneyAffairsSrv = moneyAffairsSrv;
    }


    /**
     * this method is responsible for add money affair process.
     * @param  request HttpServletRequest.
     * @param  moneyAffairRequestDTO A request DTO.
     * @param  bindingResult for bean validation.
     * @return GenericResponse a generic response of application.
     */
    @Operation(summary = "ثبت یک هزینه/درآمد")
    @PostMapping(value ="/add")
    public GenericResponse addAffair(HttpServletRequest request, @RequestBody @Validated MoneyAffairRequestDTO moneyAffairRequestDTO, BindingResult bindingResult) throws Exception{
        return moneyAffairsSrv.addAffair(request, moneyAffairRequestDTO, bindingResult);
    }


    /**
     * this method is responsible for add daily report per type process.
     * @param  request HttpServletRequest.
     * @param  dailyReportPerTypeRequestDTO A request DTO.
     * @param  bindingResult for bean validation.
     * @return GenericResponse a generic response of application.
     */
    @Operation(summary = "گزارش هزینه روزانه/درآمد روزانه")
    @PostMapping(value ="/daily-report-per-type")
    public GenericResponse dailyReportPerType(HttpServletRequest request, @RequestBody @Validated DailyReportPerTypeRequestDTO dailyReportPerTypeRequestDTO, BindingResult bindingResult) throws Exception{
        return moneyAffairsSrv.dailyReportPerType(request, dailyReportPerTypeRequestDTO, bindingResult);
    }


    /**
     * this method is responsible for add daily report process. this gives us accounting per day.
     * @param  request HttpServletRequest.
     * @param  dailyReportRequestDTO A request DTO.
     * @param  bindingResult for bean validation.
     * @return GenericResponse a generic response of application.
     */
    @Operation(summary = "گزارش جامع هزینه ها و درآمدهای روزانه")
    @PostMapping(value ="/daily-report")
    public GenericResponse dailyReport(HttpServletRequest request, @RequestBody @Validated DailyReportRequestDTO dailyReportRequestDTO, BindingResult bindingResult) throws Exception{
        return moneyAffairsSrv.dailyReport(request, dailyReportRequestDTO, bindingResult);
    }
}
