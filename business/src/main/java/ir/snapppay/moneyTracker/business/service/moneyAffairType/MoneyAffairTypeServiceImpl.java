package ir.snapppay.moneyTracker.business.service.moneyAffairType;


import ir.snapppay.moneyTracker.business.dto.moneyAffairType.MoneyAffairTypeRequestDTO;
import ir.snapppay.moneyTracker.business.dto.moneyAffairType.MoneyAffairTypeResponseDTO;
import ir.snapppay.moneyTracker.common.dto.GenericResponse;
import ir.snapppay.moneyTracker.common.enums.ResponseStatusEnum;
import ir.snapppay.moneyTracker.common.exception.MoneyTypeAlreadyExistException;
import ir.snapppay.moneyTracker.common.utils.GeneralUtils;
import ir.snapppay.moneyTracker.persistence.domain.MoneyAffairType;
import ir.snapppay.moneyTracker.persistence.domain.Users;
import ir.snapppay.moneyTracker.persistence.repository.MoneyAffairTypeRepository;
import ir.snapppay.moneyTracker.persistence.types.MoneyTypeEnum;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import java.util.Optional;

/**
 * This class has implemented the MoneyAffairTypeService.
 * It is responsible for implementing the money affair type process such as add type and ...
 * @author Behrouz Takhti
 * @version 0.0.2-SNAPSHOT
 * @since 2024-01-15
 * @see MoneyAffairTypeService
 */
@Service
public class MoneyAffairTypeServiceImpl implements MoneyAffairTypeService {

    private MoneyAffairTypeRepository moneyAffairTypeRepo;

    public MoneyAffairTypeServiceImpl(MoneyAffairTypeRepository moneyAffairTypeRepo) {
        this.moneyAffairTypeRepo = moneyAffairTypeRepo;
    }

    /**
     * this method is responsible for add money affair type process.
     * @param  request HttpServletRequest.
     * @param  moneyAffairTypeRequestDTO A request DTO.
     * @param  bindingResult for bean validation.
     * @return GenericResponse a generic response of application.
     */
    @Override
    public GenericResponse addType(HttpServletRequest request, MoneyAffairTypeRequestDTO moneyAffairTypeRequestDTO, BindingResult bindingResult) throws Exception {
        GeneralUtils.checkBindingResult(bindingResult);
        var userId = getCurrentUser().getId();
        Optional<MoneyAffairType> moneyAffairTypeInDB = moneyAffairTypeRepo.findByTypeAndNameAndUserId(MoneyTypeEnum.valueOf(moneyAffairTypeRequestDTO.getType()), moneyAffairTypeRequestDTO.getName(), userId);
        moneyAffairTypeInDB.ifPresent(users -> {
            throw new MoneyTypeAlreadyExistException("MoneyType Already exist");
        });
        var moneyTypeForSave = new MoneyAffairType();
        moneyTypeForSave.setName(moneyAffairTypeRequestDTO.getName());
        moneyTypeForSave.setPersianName(moneyAffairTypeRequestDTO.getPersianName());
        moneyTypeForSave.setDescription(moneyAffairTypeRequestDTO.getDescription());
        moneyTypeForSave.setType(MoneyTypeEnum.valueOf(moneyAffairTypeRequestDTO.getType()));
        moneyTypeForSave.setUser(new Users(userId));
        moneyAffairTypeRepo.save(moneyTypeForSave);
        MoneyAffairTypeResponseDTO moneyAffairTypeResponseDTO = new MoneyAffairTypeResponseDTO(moneyTypeForSave.getId(),
                moneyTypeForSave.getName(), moneyTypeForSave.getPersianName(), moneyTypeForSave.getType().name(), moneyTypeForSave.getDescription());
        return new GenericResponse(HttpStatus.OK.value(), ResponseStatusEnum.SUCCESS.name(), ResponseStatusEnum.SUCCESS.toMessage(), moneyAffairTypeResponseDTO);
    }
}
