package ir.snapppay.moneyTracker.business.dto.moneyAffair;

import io.swagger.v3.oas.annotations.media.Schema;
import ir.snapppay.moneyTracker.business.rest.moneyAffairType.MoneyAffairTypeController;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;

/**
 * This class is used in daily report process as request DTO;
 * @author Behrouz Takhti
 * @version 0.0.-SNAPSHOT
 * @since 2024-01-15
 * @see MoneyAffairTypeController
 */
public class DailyReportRequestDTO {


    @Schema(description = "تاریخ هزینه یا درآمد - تاریخ به میلادی و با فرمت ذکر شده", example = "2024-01-16")
    @NotBlank(message = "NotBlank.date")
    @Pattern(regexp = "^\\d{4}-\\d{2}-\\d{2}$", message = "Pattern.date")
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
