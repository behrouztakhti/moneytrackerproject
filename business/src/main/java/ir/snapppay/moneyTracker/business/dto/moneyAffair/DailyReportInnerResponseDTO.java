package ir.snapppay.moneyTracker.business.dto.moneyAffair;

import com.fasterxml.jackson.annotation.JsonInclude;
import ir.snapppay.moneyTracker.business.rest.moneyAffairType.MoneyAffairTypeController;

import java.util.ArrayList;
import java.util.List;


/**
 * this method is responsible for add daily report process. this gives us accounting per day.
 * @author Behrouz Takhti
 * @version 0.0.-SNAPSHOT
 * @since 2024-01-15
 * @see MoneyAffairTypeController
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DailyReportInnerResponseDTO {

    private List<MoneyAffairResponseDTO> expenses = new ArrayList();
    private List<MoneyAffairResponseDTO> incomes = new ArrayList();

    public List<MoneyAffairResponseDTO> getExpenses() {
        return expenses;
    }

    public void setExpenses(List<MoneyAffairResponseDTO> expenses) {
        this.expenses = expenses;
    }

    public List<MoneyAffairResponseDTO> getIncomes() {
        return incomes;
    }

    public void setIncomes(List<MoneyAffairResponseDTO> incomes) {
        this.incomes = incomes;
    }
}
