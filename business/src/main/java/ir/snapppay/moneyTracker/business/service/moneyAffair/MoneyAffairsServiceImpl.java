package ir.snapppay.moneyTracker.business.service.moneyAffair;


import ir.snapppay.moneyTracker.business.dto.moneyAffair.*;
import ir.snapppay.moneyTracker.common.dto.GenericResponse;
import ir.snapppay.moneyTracker.common.enums.ResponseStatusEnum;
import ir.snapppay.moneyTracker.common.exception.GeneralException;
import ir.snapppay.moneyTracker.common.exception.MoneyTypeNotFoundException;
import ir.snapppay.moneyTracker.common.utils.GeneralUtils;
import ir.snapppay.moneyTracker.persistence.domain.MoneyAffairType;
import ir.snapppay.moneyTracker.persistence.domain.MoneyAffairs;
import ir.snapppay.moneyTracker.persistence.repository.MoneyAffairTypeRepository;
import ir.snapppay.moneyTracker.persistence.repository.MoneyAffairsRepository;
import ir.snapppay.moneyTracker.persistence.types.MoneyTypeEnum;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;


/**
 * This class has implemented the MoneyAffairsService.
 * It is responsible for implementing the money affair process such as add and ...
 * @author Behrouz Takhti
 * @version 0.0.3-SNAPSHOT
 * @since 2024-01-15
 * @see MoneyAffairsService
 */
@Service
public class MoneyAffairsServiceImpl implements MoneyAffairsService{

    private MoneyAffairTypeRepository moneyAffairTypeRepo;
    private MoneyAffairsRepository moneyAffairsRepo;

    public MoneyAffairsServiceImpl(MoneyAffairTypeRepository moneyAffairTypeRepo, MoneyAffairsRepository moneyAffairsRepo) {
        this.moneyAffairTypeRepo = moneyAffairTypeRepo;
        this.moneyAffairsRepo = moneyAffairsRepo;
    }


    /**
     * this method is responsible for add money affair process.
     * @param  request HttpServletRequest.
     * @param  moneyAffairRequestDTO A request DTO.
     * @param  bindingResult for bean validation.
     * @return GenericResponse a generic response of application.
     */
    @Override
    public GenericResponse addAffair(HttpServletRequest request, MoneyAffairRequestDTO moneyAffairRequestDTO, BindingResult bindingResult) throws Exception {
        var userId = getCurrentUser().getId();
        GeneralUtils.checkBindingResult(bindingResult);
        Optional<MoneyAffairType> moneyAffairTypeInDB = moneyAffairTypeRepo.findById(moneyAffairRequestDTO.getMoneyAffairTypeID());
        if (moneyAffairTypeInDB.isEmpty()){
            throw new MoneyTypeNotFoundException("MoneyType not found");
        }
        if (!moneyAffairTypeInDB.get().getUser().getId().equals(userId)){
            throw new GeneralException("moneyAffairType not belongs to this user !");
        }
        var moneyAffairForSave = new MoneyAffairs();
        moneyAffairForSave.setAmount(moneyAffairRequestDTO.getAmount());
        moneyAffairForSave.setDescription(moneyAffairRequestDTO.getDescription());
        moneyAffairForSave.setMoneyAffairType(moneyAffairTypeInDB.get());
        moneyAffairsRepo.save(moneyAffairForSave);
        MoneyAffairResponseDTO moneyAffairResponseDTO = new MoneyAffairResponseDTO(moneyAffairForSave.getId(), moneyAffairForSave.getDescription(),
                moneyAffairForSave.getAmount(), moneyAffairForSave.getMoneyAffairType().getId(), moneyAffairForSave.getMoneyAffairType().getName(), moneyAffairForSave.getCreatedDate().toString());
        return new GenericResponse(HttpStatus.OK.value(), ResponseStatusEnum.SUCCESS.name(), ResponseStatusEnum.SUCCESS.toMessage(), moneyAffairResponseDTO);
    }

    /**
     * this method is responsible for add daily report per type process.
     * @param  request HttpServletRequest.
     * @param  dailyReportPerTypeRequestDTO A request DTO.
     * @param  bindingResult for bean validation.
     * @return GenericResponse a generic response of application.
     */
    @Override
    public GenericResponse dailyReportPerType(HttpServletRequest request, DailyReportPerTypeRequestDTO dailyReportPerTypeRequestDTO, BindingResult bindingResult) throws Exception {
        GeneralUtils.checkBindingResult(bindingResult);
        var userId = getCurrentUser().getId();
        Optional<MoneyAffairType> moneyAffairTypeInDB = moneyAffairTypeRepo.findById(dailyReportPerTypeRequestDTO.getMoneyAffairTypeID());
        if (moneyAffairTypeInDB.isEmpty()){
            throw new MoneyTypeNotFoundException("MoneyType not found");
        }
        if (!moneyAffairTypeInDB.get().getUser().getId().equals(userId)){
            throw new GeneralException("moneyAffairType not belongs to this user !");
        }
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(dailyReportPerTypeRequestDTO.getDate(), dtf);
        List<MoneyAffairs> moneyAffairs = moneyAffairsRepo.findByCreatedDateAndMoneyAffairTypeId(
                date,dailyReportPerTypeRequestDTO.getMoneyAffairTypeID()
        );
        List<MoneyAffairResponseDTO> affairs = moneyAffairs.stream().map(item ->{
            MoneyAffairResponseDTO dt = new MoneyAffairResponseDTO(item.getId(), item.getDescription(), item.getAmount(),
                    null,null, null);
            return dt;
        }).toList();
        BigDecimal totlaAmount = moneyAffairs.stream().map(MoneyAffairs::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
        DailyReportPerTypeResponseDTO responseDTO = new DailyReportPerTypeResponseDTO(
                        dailyReportPerTypeRequestDTO.getDate(), moneyAffairTypeInDB.get().getName(), moneyAffairTypeInDB.get().getType().name(), totlaAmount, affairs);
        return new GenericResponse(HttpStatus.OK.value(), ResponseStatusEnum.SUCCESS.name(), ResponseStatusEnum.SUCCESS.toMessage(), responseDTO);
    }



    /**
     * this method is responsible for add daily report process. this gives us accounting per day.
     * @param  request HttpServletRequest.
     * @param  dailyReportRequestDTO A request DTO.
     * @param  bindingResult for bean validation.
     * @return GenericResponse a generic response of application.
     */
    @Override
    public GenericResponse dailyReport(HttpServletRequest request, DailyReportRequestDTO dailyReportRequestDTO, BindingResult bindingResult) throws Exception {
        var userId = getCurrentUser().getId();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(dailyReportRequestDTO.getDate(), dtf);
        List<MoneyAffairs> ls = moneyAffairsRepo.findByCreatedDateAndMoneyAffairType_UserId(date, userId);;
        BigDecimal totalIncome = ls.stream().filter(item -> item.getMoneyAffairType().getType().equals(MoneyTypeEnum.INCOME))
                .map(MoneyAffairs::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);

        BigDecimal totalExpense = ls.stream().filter(item -> item.getMoneyAffairType().getType().equals(MoneyTypeEnum.EXPENSE))
                .map(MoneyAffairs::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);


        DailyReportInnerResponseDTO affairs = new DailyReportInnerResponseDTO();
        affairs.setIncomes(ls.stream().filter(item -> item.getMoneyAffairType().getType().equals(MoneyTypeEnum.INCOME))
                .map(item -> {
                    return new MoneyAffairResponseDTO(item.getId(), item.getDescription(), item.getAmount(),
                            item.getMoneyAffairType().getId(), item.getMoneyAffairType().getName(), null);
                }).toList());
        affairs.setExpenses(ls.stream().filter(item -> item.getMoneyAffairType().getType().equals(MoneyTypeEnum.EXPENSE))
                .map(item -> {
                    return new MoneyAffairResponseDTO(item.getId(), item.getDescription(), item.getAmount(),
                            item.getMoneyAffairType().getId(), item.getMoneyAffairType().getName(), null);
                }).toList());

        DailyReportResponseDTO responseDTO = new DailyReportResponseDTO(dailyReportRequestDTO.getDate(), totalIncome, totalExpense, affairs);
        return new GenericResponse(HttpStatus.OK.value(), ResponseStatusEnum.SUCCESS.name(), ResponseStatusEnum.SUCCESS.toMessage(), responseDTO);
    }
}
