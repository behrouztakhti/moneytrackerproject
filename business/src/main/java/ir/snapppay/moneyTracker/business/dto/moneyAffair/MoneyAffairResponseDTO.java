package ir.snapppay.moneyTracker.business.dto.moneyAffair;

import com.fasterxml.jackson.annotation.JsonInclude;
import ir.snapppay.moneyTracker.business.rest.moneyAffairType.MoneyAffairTypeController;

import java.math.BigDecimal;
import java.time.LocalDateTime;



/**
 * This class is used in User-add money affair process as request DTO;
 * @author Behrouz Takhti
 * @version 0.0.-SNAPSHOT
 * @since 2024-01-15
 * @see MoneyAffairTypeController
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public record MoneyAffairResponseDTO(Long id, String description, BigDecimal amount, Long moneyAffairTypeId, String moneyAffairTypeName, String createdDate) {
}
