package ir.snapppay.moneyTracker.business.rest.moneyAffairType;

import io.swagger.v3.oas.annotations.Operation;
import ir.snapppay.moneyTracker.business.dto.moneyAffairType.MoneyAffairTypeRequestDTO;
import ir.snapppay.moneyTracker.business.service.moneyAffairType.MoneyAffairTypeService;
import ir.snapppay.moneyTracker.common.dto.GenericResponse;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * This controller is responsible for money affair type process such as add type and ...
 * @author Behrouz Takhti
 * @version 0.0.2-SNAPSHOT
 * @since 2024-01-15
 */
@RestController
@RequestMapping(value = "/money-affair-type")
public class MoneyAffairTypeController {

    private MoneyAffairTypeService moneyAffairTypeSrv;

    public MoneyAffairTypeController(MoneyAffairTypeService moneyAffairTypeSrv) {
        this.moneyAffairTypeSrv = moneyAffairTypeSrv;
    }



    /**
     * this method is responsible for add money affair type process.
     * @param  request HttpServletRequest.
     * @param  moneyAffairTypeRequestDTO A request DTO.
     * @param  bindingResult for bean validation.
     * @return GenericResponse a generic response of application.
     */
    @Operation(summary = "ثبت یک نوع هزیته/درآمد")
    @PostMapping(value ="/add")
    public GenericResponse addType(HttpServletRequest request, @RequestBody @Validated MoneyAffairTypeRequestDTO moneyAffairTypeRequestDTO, BindingResult bindingResult) throws Exception{
        return moneyAffairTypeSrv.addType(request, moneyAffairTypeRequestDTO, bindingResult);
    }

}
