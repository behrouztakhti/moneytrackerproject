package ir.snapppay.moneyTracker.business.dto.moneyAffair;

import io.swagger.v3.oas.annotations.media.Schema;
import ir.snapppay.moneyTracker.business.rest.moneyAffairType.MoneyAffairTypeController;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;

import java.math.BigDecimal;

/**
 * This class is used in User-add money affair process as request DTO;
 * @author Behrouz Takhti
 * @version 0.0.-SNAPSHOT
 * @since 2024-01-15
 * @see MoneyAffairTypeController
 */
public class MoneyAffairRequestDTO {

    @Schema(description = "مبلغ هزینه یا درآمد", example = "1500")
    @NotNull(message = "NotNull.amount")
    @Positive(message = "Positive.amount")
    private BigDecimal amount;

    @Schema(description = "شناسه نوع هزینه یا درآمد", example = "1")
    @NotNull(message = "NotNull.moneyAffairTypeID")
    private Long moneyAffairTypeID;

    @Schema(description = "توضیحات", example = "توضیحات")
    @NotBlank(message = "NotBlank.description")
    private String description;


    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getMoneyAffairTypeID() {
        return moneyAffairTypeID;
    }

    public void setMoneyAffairTypeID(Long moneyAffairTypeID) {
        this.moneyAffairTypeID = moneyAffairTypeID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
