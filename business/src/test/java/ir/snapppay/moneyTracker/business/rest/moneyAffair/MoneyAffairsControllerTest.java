package ir.snapppay.moneyTracker.business.rest.moneyAffair;
import ir.snapppay.moneyTracker.business.dto.moneyAffair.DailyReportPerTypeRequestDTO;
import ir.snapppay.moneyTracker.business.dto.moneyAffair.DailyReportRequestDTO;
import ir.snapppay.moneyTracker.business.dto.moneyAffair.MoneyAffairRequestDTO;
import ir.snapppay.moneyTracker.business.service.moneyAffair.MoneyAffairsService;
import ir.snapppay.moneyTracker.common.test.SetUpTest;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.BindingResult;
import java.math.BigDecimal;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles(value = "test")
@WebMvcTest(controllers = MoneyAffairsController.class )
@ContextConfiguration(classes = MoneyAffairsController.class)
public class MoneyAffairsControllerTest extends SetUpTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    MoneyAffairsService moneyAffairsSrv;



    ///////////////////////// --------- /money-affairs/add ---------  ////////////////////
    @Test
    @DisplayName("addAffair_whenUserIsNotAuthenticated_thenReturns401")
    public void addAffair_whenUserIsNotAuthenticated_thenReturns401() throws Exception{
        var dto = new MoneyAffairRequestDTO();
        dto.setDescription("توضیحات");
        dto.setAmount(new BigDecimal(1500));
        dto.setMoneyAffairTypeID(1L);
        mockMvc.perform(
                        post("/money-affairs/add")
                                .with(csrf())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectWriter.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(value = "firstUser")
    @DisplayName("addAffair_whenUserIsAuthenticatedButNotAuthorized_thenReturns403")
    public void addAffair_whenUserIsAuthenticatedButNotAuthorized_thenReturns403() throws Exception{
        var dto = new MoneyAffairRequestDTO();
        dto.setDescription("توضیحات");
        dto.setAmount(new BigDecimal(1500));
        dto.setMoneyAffairTypeID(1L);
        mockMvc.perform(
                        post("/money-affairs/add")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectWriter.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(value = "firstUser")
    @DisplayName("addAffair_whenUrlIsNotValid_thenReturns404")
    public void addAffair_whenUrlIsNotValid_thenReturns404() throws Exception{
        var dto = new MoneyAffairRequestDTO();
        dto.setDescription("توضیحات");
        dto.setAmount(new BigDecimal(1500));
        dto.setMoneyAffairTypeID(1L);
        mockMvc.perform(
                        post("/money-affairs/addAS")
                                .with(csrf())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectWriter.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(value = "firstUser")
    @DisplayName("addAffair_whenValidInput_thenReturns200")
    public void addAffair_whenValidInput_thenReturns200() throws Exception{
        var dto = new MoneyAffairRequestDTO();
        dto.setDescription("توضیحات");
        dto.setAmount(new BigDecimal(1500));
        dto.setMoneyAffairTypeID(1L);
        mockMvc.perform(
                        post("/money-affairs/add")
                                .with(csrf())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectWriter.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(value = "firstUser")
    @DisplayName("addAffair_whenValidInput_thenMapsToBusinessModel")
    public void addAffair_whenValidInput_thenMapsToBusinessModel() throws Exception{
        var dto = new MoneyAffairRequestDTO();
        dto.setDescription("توضیحات");
        dto.setAmount(new BigDecimal(1500));
        dto.setMoneyAffairTypeID(1L);
        mockMvc.perform(
                        post("/money-affairs/add")
                                .with(csrf())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectWriter.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isOk());
        ArgumentCaptor<HttpServletRequest> HttpArgumentCaptor = ArgumentCaptor.forClass(HttpServletRequest.class);
        ArgumentCaptor<MoneyAffairRequestDTO> requestDtoArgumentCaptor = ArgumentCaptor.forClass(MoneyAffairRequestDTO.class);
        ArgumentCaptor<BindingResult> bindingResultArgumentCaptor = ArgumentCaptor.forClass(BindingResult.class);
        verify(moneyAffairsSrv, times(1)).addAffair(HttpArgumentCaptor.capture(), requestDtoArgumentCaptor.capture(), bindingResultArgumentCaptor.capture());
        assertThat(requestDtoArgumentCaptor.getValue().getAmount()).isEqualTo(new BigDecimal(1500));
        assertThat(requestDtoArgumentCaptor.getValue().getDescription()).isEqualTo("توضیحات");
        assertThat(requestDtoArgumentCaptor.getValue().getMoneyAffairTypeID()).isEqualTo(1L);
    }


    ///////////////////////// --------- money-affairs/daily-report-per-type ---------  ////////////////////
    @Test
    @DisplayName("dailyReportPerType_whenUserIsNotAuthenticated_thenReturns401")
    public void dailyReportPerType_whenUserIsNotAuthenticated_thenReturns401() throws Exception{
        var dto = new DailyReportPerTypeRequestDTO();
        dto.setDate("2024-01-16");
        dto.setMoneyAffairTypeID(1L);
        mockMvc.perform(
                        post("/money-affairs/daily-report-per-type")
                                .with(csrf())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectWriter.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }
    @Test
    @WithMockUser(value = "firstUser")
    @DisplayName("dailyReportPerType_whenUserIsAuthenticatedButNotAuthorized_thenReturns403")
    public void dailyReportPerType_whenUserIsAuthenticatedButNotAuthorized_thenReturns403() throws Exception{
        var dto = new DailyReportPerTypeRequestDTO();
        dto.setDate("2024-01-16");
        dto.setMoneyAffairTypeID(1L);
        mockMvc.perform(
                        post("/money-affairs/daily-report-per-type")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectWriter.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isForbidden());
    }
    @Test
    @WithMockUser(value = "firstUser")
    @DisplayName("dailyReportPerType_whenUrlIsNotValid_thenReturns404")
    public void dailyReportPerType_whenUrlIsNotValid_thenReturns404() throws Exception{
        var dto = new DailyReportPerTypeRequestDTO();
        dto.setDate("2024-01-16");
        dto.setMoneyAffairTypeID(1L);
        mockMvc.perform(
                        post("/money-affairs/daily-report-per-typeasas")
                                .with(csrf())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectWriter.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
    @Test
    @WithMockUser(value = "firstUser")
    @DisplayName("dailyReportPerType_whenValidInput_thenReturns200")
    public void dailyReportPerType_whenValidInput_thenReturns200() throws Exception{
        var dto = new DailyReportPerTypeRequestDTO();
        dto.setDate("2024-01-16");
        dto.setMoneyAffairTypeID(1L);
        mockMvc.perform(
                        post("/money-affairs/daily-report-per-type")
                                .with(csrf())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectWriter.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isOk());
    }
    @Test
    @WithMockUser(value = "firstUser")
    @DisplayName("dailyReportPerType_whenValidInput_thenMapsToBusinessModel")
    public void dailyReportPerType_whenValidInput_thenMapsToBusinessModel() throws Exception{
        var dto = new DailyReportPerTypeRequestDTO();
        dto.setDate("2024-01-16");
        dto.setMoneyAffairTypeID(1L);
        mockMvc.perform(
                        post("/money-affairs/daily-report-per-type")
                                .with(csrf())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectWriter.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isOk());
        ArgumentCaptor<HttpServletRequest> HttpArgumentCaptor = ArgumentCaptor.forClass(HttpServletRequest.class);
        ArgumentCaptor<DailyReportPerTypeRequestDTO> requestDtoArgumentCaptor = ArgumentCaptor.forClass(DailyReportPerTypeRequestDTO.class);
        ArgumentCaptor<BindingResult> bindingResultArgumentCaptor = ArgumentCaptor.forClass(BindingResult.class);
        verify(moneyAffairsSrv, times(1)).dailyReportPerType(HttpArgumentCaptor.capture(), requestDtoArgumentCaptor.capture(), bindingResultArgumentCaptor.capture());
        assertThat(requestDtoArgumentCaptor.getValue().getDate()).isEqualTo("2024-01-16");
        assertThat(requestDtoArgumentCaptor.getValue().getMoneyAffairTypeID()).isEqualTo(1L);
    }

    ///////////////////////// --------- money-affairs/daily-report ---------  ////////////////////
    @Test
    @DisplayName("dailyReport_whenUserIsNotAuthenticated_thenReturns401")
    public void dailyReport_whenUserIsNotAuthenticated_thenReturns401() throws Exception{
        var dto = new DailyReportRequestDTO();
        dto.setDate("2024-01-16");
        mockMvc.perform(
                        post("/money-affairs/daily-report")
                                .with(csrf())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectWriter.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }
    @Test
    @WithMockUser(value = "firstUser")
    @DisplayName("dailyReport_whenUserIsAuthenticatedButNotAuthorized_thenReturns403")
    public void dailyReport_whenUserIsAuthenticatedButNotAuthorized_thenReturns403() throws Exception{
        var dto = new DailyReportRequestDTO();
        dto.setDate("2024-01-16");
        mockMvc.perform(
                        post("/money-affairs/daily-report")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectWriter.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isForbidden());
    }
    @Test
    @WithMockUser(value = "firstUser")
    @DisplayName("dailyReport_whenUrlIsNotValid_thenReturns404")
    public void dailyReport_whenUrlIsNotValid_thenReturns404() throws Exception{
        var dto = new DailyReportRequestDTO();
        dto.setDate("2024-01-16");
        mockMvc.perform(
                        post("/money-affairs/daily-reportt")
                                .with(csrf())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectWriter.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
    @Test
    @WithMockUser(value = "firstUser")
    @DisplayName("dailyReport_whenValidInput_thenReturns200")
    public void dailyReport_whenValidInput_thenReturns200() throws Exception{
        var dto = new DailyReportRequestDTO();
        dto.setDate("2024-01-16");
        mockMvc.perform(
                        post("/money-affairs/daily-report")
                                .with(csrf())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectWriter.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isOk());
    }
    @Test
    @WithMockUser(value = "firstUser")
    @DisplayName("dailyReport_whenValidInput_thenMapsToBusinessModel")
    public void dailyReport_whenValidInput_thenMapsToBusinessModel() throws Exception{
        var dto = new DailyReportRequestDTO();
        dto.setDate("2024-01-16");
        mockMvc.perform(
                        post("/money-affairs/daily-report")
                                .with(csrf())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectWriter.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isOk());
        ArgumentCaptor<HttpServletRequest> HttpArgumentCaptor = ArgumentCaptor.forClass(HttpServletRequest.class);
        ArgumentCaptor<DailyReportRequestDTO> requestDtoArgumentCaptor = ArgumentCaptor.forClass(DailyReportRequestDTO.class);
        ArgumentCaptor<BindingResult> bindingResultArgumentCaptor = ArgumentCaptor.forClass(BindingResult.class);
        verify(moneyAffairsSrv, times(1)).dailyReport(HttpArgumentCaptor.capture(), requestDtoArgumentCaptor.capture(), bindingResultArgumentCaptor.capture());
        assertThat(requestDtoArgumentCaptor.getValue().getDate()).isEqualTo("2024-01-16");
    }







}
