package ir.snapppay.moneyTracker.business.rest.moneyAffairType;


import ir.snapppay.moneyTracker.business.dto.moneyAffairType.MoneyAffairTypeRequestDTO;
import ir.snapppay.moneyTracker.business.service.moneyAffairType.MoneyAffairTypeService;
import ir.snapppay.moneyTracker.common.test.SetUpTest;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.BindingResult;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles(value = "test")
@WebMvcTest(controllers = MoneyAffairTypeController.class )
@ContextConfiguration(classes = MoneyAffairTypeController.class)
public class MoneyAffairTypeControllerTest extends SetUpTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    MoneyAffairTypeService moneyAffairTypeSrv;

    ///////////////////////// --------- /money-affair-type/add ---------  ////////////////////
    @Test
    @DisplayName("addAffairType_whenUserIsNotAuthenticated_thenReturns401")
    public void addAffairType_whenUserIsNotAuthenticated_thenReturns401() throws Exception{
        var dto = new MoneyAffairTypeRequestDTO();
        dto.setDescription("هزینه صبحانه");
        dto.setType("EXPENSE");
        dto.setName("breakfast");
        dto.setPersianName("صبحانه");
        mockMvc.perform(
                        post("/money-affair-type/add")
                                .with(csrf())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectWriter.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }
    @Test
    @WithMockUser(value = "firstUser")
    @DisplayName("addAffairType_whenUserIsAuthenticatedButNotAuthorized_thenReturns403")
    public void addAffairType_whenUserIsAuthenticatedButNotAuthorized_thenReturns403() throws Exception{
        var dto = new MoneyAffairTypeRequestDTO();
        dto.setDescription("هزینه صبحانه");
        dto.setType("EXPENSE");
        dto.setName("breakfast");
        dto.setPersianName("صبحانه");
        mockMvc.perform(
                        post("/money-affair-type/add")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectWriter.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isForbidden());
    }
    @Test
    @WithMockUser(value = "firstUser")
    @DisplayName("addAffairType_whenUrlIsNotValid_thenReturns404")
    public void addAffairType_whenUrlIsNotValid_thenReturns404() throws Exception{
        var dto = new MoneyAffairTypeRequestDTO();
        dto.setDescription("هزینه صبحانه");
        dto.setType("EXPENSE");
        dto.setName("breakfast");
        dto.setPersianName("صبحانه");
        mockMvc.perform(
                        post("/money-affair-type/addd")
                                .with(csrf())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectWriter.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
    @Test
    @WithMockUser(value = "firstUser")
    @DisplayName("addAffairType_whenValidInput_thenReturns200")
    public void addAffairType_whenValidInput_thenReturns200() throws Exception{
        var dto = new MoneyAffairTypeRequestDTO();
        dto.setDescription("هزینه صبحانه");
        dto.setType("EXPENSE");
        dto.setName("breakfast");
        dto.setPersianName("صبحانه");
        mockMvc.perform(
                        post("/money-affair-type/add")
                                .with(csrf())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectWriter.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isOk());
    }
    @Test
    @WithMockUser(value = "firstUser")
    @DisplayName("addAffairType_whenValidInput_thenMapsToBusinessModel")
    public void addAffairType_whenValidInput_thenMapsToBusinessModel() throws Exception{
        var dto = new MoneyAffairTypeRequestDTO();
        dto.setDescription("هزینه صبحانه");
        dto.setType("EXPENSE");
        dto.setName("breakfast");
        dto.setPersianName("صبحانه");
        mockMvc.perform(
                        post("/money-affair-type/add")
                                .with(csrf())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectWriter.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isOk());
        ArgumentCaptor<HttpServletRequest> HttpArgumentCaptor = ArgumentCaptor.forClass(HttpServletRequest.class);
        ArgumentCaptor<MoneyAffairTypeRequestDTO> requestDtoArgumentCaptor = ArgumentCaptor.forClass(MoneyAffairTypeRequestDTO.class);
        ArgumentCaptor<BindingResult> bindingResultArgumentCaptor = ArgumentCaptor.forClass(BindingResult.class);
        verify(moneyAffairTypeSrv, times(1)).addType(HttpArgumentCaptor.capture(), requestDtoArgumentCaptor.capture(), bindingResultArgumentCaptor.capture());
        assertThat(requestDtoArgumentCaptor.getValue().getDescription()).isEqualTo("هزینه صبحانه");
        assertThat(requestDtoArgumentCaptor.getValue().getType()).isEqualTo("EXPENSE");
        assertThat(requestDtoArgumentCaptor.getValue().getName()).isEqualTo("breakfast");
        assertThat(requestDtoArgumentCaptor.getValue().getPersianName()).isEqualTo("صبحانه");
    }

}
